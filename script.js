/* Відповіді:

1.  Метод об'єкту це функція, яка прописується у об'єкті.

2.  Значення властивості об'єкту може мати будь-який тип данних.
    
3.  Це означає, що значенням будь-якого об'єкту є посилання на інше значення.
*/

function createNewUser(firstName, lastName){
    this.firstName = firstName,
    this.lastName = lastName,
    this.getLogin = function(){
        console.log('Рекомендований пароль: ' + this.firstName[0].toLowerCase() + this.lastName.toLowerCase()); 
    } 
}

let firstName = prompt('First name?');
let lastName = prompt('Last name?');

let newUser = new createNewUser(firstName, lastName);
console.log(newUser);
newUser.getLogin();